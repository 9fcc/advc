# Advanced C

## Name
Advanced C Libraries.

Short name used for packages: advc.

## Description
The Advanced C libraries are intended to abstract the standard C library and POSIX interface with more accurate and universal interface, that provide all necessary functions that typical application will need.

## Dependencies
To build libraries you need to install:
* cmake
* gcc or clang
* check (for tests).

To install dependencies in Debian run:
```shell
sudo apt update
sudo apt install -y build-essential check cmake
```

## Clone and build

To compile and install into `build/root` directory run the following commands:
```shell
git clone https://gitlab.com/andrdots/advc.git ./source
mkdir ./build && cd ./build
cmake -DCMAKE_INSTALL_PREFIX=./root ../source && make install
```

## Installation
The project supports packaging through CPack utility. Packages can be build with the following command:

```shell
python ${SOURCE_DIR}/builder/acoby/acoby_run.py --data-path=${SOURCE_DIR}/builder --source-path=/home/arch/opensource/utils/source --build-path=${BUILD_DIR} --distr=${DISTR} --codename=${DISTR_CODENAME} --arch=${ARCHITECTURE} --tool cmake -- package
```

Where:
* SOURCE_DIR — directory with the advc libraries.
* BUILD_DIR — directory where build process will take place.
* DISTR — linux distribution name. Supported values: ubuntu, debian.
* DISTR_CODENAME — codename of the linux distribution.
* ARCHITECTURE — architecture of the Linux distribution. Supported values (depending on the distribution): amd64, i386.

## Contributing
For the period of initial development the project is closed for contributions. Feel free to make forks for your own needs.

## Authors and acknowledgment
Author: Andrey Dotsenko.

## License
Advanced C libraries are licensed under LGPL 2.1.

## Project status
The project is under development. Currently it is not recommended for any usage in non-test applications.

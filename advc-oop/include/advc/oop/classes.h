// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2024 Andrey N. Dotsenko
 */

#ifndef H_ADVC_OOP_CLASSES_H_
#define H_ADVC_OOP_CLASSES_H_

#include <advc/types.h>
#include <stdatomic.h>
#include <threads.h>
#include <stdlib.h>

typedef struct a_object_t a_object_t;
typedef struct a_object_type_t a_object_type_t;

struct a_object_type_t {
    //! Class identifier within the current process
    a_number_t id;

    //! Parent classes
    const a_object_type_t **parent_types;

    //! Parent classes count
    a_many_t parents_count;

    //! Class instance size (only a struct size)
    a_size_t instance_size;

    //! Name of the class
    a_str_t name;

    //! Type struct size
    a_size_t type_size;

    //! Class default constructor
    errno_t (*initialize)(a_object_t *object);

    //! Class default destructor
    void (*finalize)(a_object_t *object);
};

struct a_object_t {
    const a_object_type_t *type;
    _Atomic(a_many_t) reference_count;
};

//! Base object type
const a_object_type_t *a_object_type();

//! Inherit class from another
errno_t a_object_type_inherit_class(a_object_type_t *type, const a_object_type_t *parent_type);

//! Register class globally and set its name
errno_t a_register_object_type(a_object_type_t *type, const a_str_t *name);

//! Finalize type (frees its internal data)
void a_object_type_finalize(a_object_type_t *type);

//! Construct object instance by its type
errno_t a_object_construct(const a_object_type_t *type, a_object_t **object_out);

//! Get object type
static inline const a_object_type_t *a_object_get_type(const a_object_t *this)
{
    return this->type;
}

//! Increase the object's reference counter
void a_object_ref(a_object_t *object);

//! Decrease the object's reference counter and free if it has zero references
void a_object_unref(a_object_t *object);

//! Is the object an instance of the specified class?
bool a_object_is_instance_of(const a_object_t *object, const a_object_type_t *target_type);

//! Define new object type that inherits from another one
#define A_IMPLEMENT_OBJECT_TYPE(prefix_lower, name_lower, prefix_upper, name_upper, parent_prefix, parent_name) \
    static errno_t initialize_##name_lower(a_object_t *this);\
    static void finalize_##name_lower(a_object_t *this);\
    static void initialize_##name_lower##_type(a_object_type_t *object_type);\
    \
    static prefix_lower##_##name_lower##_type_t this_type = {};\
    \
    prefix_lower##_##name_lower##_type_t *prefix_lower##_##name_lower##_type(void)\
    {\
        static atomic_bool is_initialized = false;\
        if (is_initialized) {\
            return &this_type;\
        }\
        \
        static atomic_bool is_initializing = false;\
        if (atomic_exchange(&is_initializing, true)) {\
            while (!is_initialized) {\
                thrd_yield();\
            }\
            return &this_type;\
        }\
        \
        errno_t r;\
        \
        a_object_type_t *object_type = (a_object_type_t *) &this_type;\
        const parent_prefix##_##parent_name##_type_t *parent_type = parent_prefix##_##parent_name##_type();\
        \
        r = a_object_type_inherit_class(object_type, (a_object_type_t *) parent_type);\
        if (r) {\
            abort();\
        }\
        \
        object_type->instance_size = sizeof(prefix_lower##_##name_lower##_t);\
        object_type->type_size = sizeof(prefix_lower##_##name_lower##_type_t);\
        \
        r = a_register_object_type(object_type, A_STR_P_BY_LITERAL(#name_lower));\
        if (r) {\
            a_object_type_finalize(object_type);\
            abort();\
        }\
        object_type->initialize = initialize_##name_lower;\
        object_type->finalize = finalize_##name_lower;\
        \
        initialize_##name_lower##_type(object_type);\
        \
        is_initialized = true;\
        return &this_type;\
    }\

#define A_DECLARE_OBJECT_TYPE(prefix_lower, name_lower, prefix_upper, name_upper, parent_prefix, parent_name) \
    prefix_lower##_##name_lower##_type_t *prefix_lower##_##name_lower##_type(void);\
    \
    static inline void prefix_lower##_##name_lower##_ref(prefix_lower##_##name_lower##_t *this)\
    {\
        parent_prefix##_##parent_name##_ref(&this->parent);\
    }\
    \
    static inline void prefix_lower##_##name_lower##_unref(prefix_lower##_##name_lower##_t *this)\
    {\
        parent_prefix##_##parent_name##_unref(&this->parent);\
    }\
    \
    static inline const prefix_lower##_##name_lower##_type_t *prefix_lower##_##name_lower##_get_type(const prefix_lower##_##name_lower##_t *this)\
    {\
        return (prefix_lower##_##name_lower##_type_t *) a_object_get_type((a_object_t *) this);\
    }


#endif /* H_ADVC_OOP_CLASSES_H_ */

// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2024 Andrey N. Dotsenko
 */

#include <advc/oop/classes.h>

#include <advc/mem.h>

#include <stdlib.h>
#include <memory.h>

static errno_t initialize_object(a_object_t *object);
static void finalize_object(a_object_t *object);

//! Last class identifier
static _Atomic(a_number_t) last_class_id = 0;

static a_object_type_t object_type = {
    .id = 0,
    .instance_size = sizeof(a_object_t),
    .type_size = sizeof(a_object_type_t),
    .parents_count = 0,
    .parent_types = NULL,
    .name = A_STR_BY_LITERAL("object"),
    .initialize = initialize_object,
    .finalize = finalize_object,
};

const a_object_type_t *a_object_type()
{
    return &object_type;
}

errno_t a_object_type_inherit_class(a_object_type_t *type, const a_object_type_t *parent_type)
{
    memcpy(type, parent_type, parent_type->type_size);
    type->parents_count = parent_type->parents_count + 1;

    errno_t r;
    r = a_mem_alloc_array(&type->parent_types, type->parents_count);
    a_except(r);

    if (parent_type->parent_types) {
        memcpy(type->parent_types, parent_type->parent_types, sizeof(*type->parent_types) * parent_type->parents_count);
    }
    type->parent_types[type->parents_count - 1] = parent_type;

    return A_ERR_OK;
}

void a_object_type_finalize(a_object_type_t *type)
{
    a_mem_free(type->parent_types);
}

errno_t a_register_object_type(a_object_type_t *type, const a_str_t *name)
{
    type->id = ++last_class_id;
    type->name = *name;
    return A_ERR_OK;
}

static errno_t initialize_object(a_object_t *object)
{
    object->reference_count = 1;
    return A_ERR_OK;
}


static void finalize_object(a_object_t *object)
{
}


errno_t a_object_construct(const a_object_type_t *type, a_object_t **object_out)
{
    errno_t r;

    a_object_t *object;
    r = a_mem_alloc(&object, type->instance_size);
    a_except(r);

    object->type = type;

    const a_object_type_t **last_parent = &type->parent_types[type->parents_count];
    const a_object_type_t **curr_parent = &type->parent_types[0];
    for (; curr_parent != last_parent; ++curr_parent) {
        r = (*curr_parent)->initialize(object);
        a_except_goto(r, undo_parents_initialize);
    }

    r = type->initialize(object);
    a_except_goto(r, undo_parents_initialize);

    *object_out = object;
    return A_ERR_OK;

undo_parents_initialize:
    if (curr_parent != &type->parent_types[0]) {
        for (--curr_parent; curr_parent != &type->parent_types[0]; --curr_parent) {
            (*curr_parent)->finalize(object);
        }
        (*curr_parent)->finalize(object);
    }
    a_mem_free(object);
    return r;
}

void a_object_destroy(a_object_t *object)
{
    const a_object_type_t *type = object->type;
    const a_object_type_t **first_parent = &type->parent_types[0];
    for (const a_object_type_t **curr_parent = &type->parent_types[type->parents_count - 1]; curr_parent != first_parent; --curr_parent) {
        (*curr_parent)->finalize(object);
    }
    a_mem_free(object);
}

void a_object_ref(a_object_t *object)
{
    ++object->reference_count;
}

void a_object_unref(a_object_t *object)
{
    a_many_t old_ref = atomic_fetch_sub(&object->reference_count, 1);
    if (old_ref == 1) {
        a_object_destroy(object);
    }
}

bool a_object_is_instance_of(const a_object_t *object, const a_object_type_t *target_type)
{
    const a_object_type_t *object_type = object->type;
    if (object_type == target_type) {
        return true;
    }
    a_many_t target_id = target_type->id;

    a_many_t start_i = 0;
    a_many_t end_i = object_type->parents_count - 1;
    while (start_i < end_i) {
        a_many_t curr_i = (start_i + end_i) / 2;
        a_many_t curr_id = object_type->parent_types[curr_i]->id;
        if (target_id <= curr_id) {
            end_i = curr_i;
        } else {
            start_i = curr_i + 1;
        }
    }

    return object_type->parent_types[start_i]->id == target_id;
}

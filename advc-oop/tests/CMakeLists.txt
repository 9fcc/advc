find_package(PkgConfig)

pkg_check_modules(CHECK REQUIRED
    check
IMPORTED_TARGET
)

declare_executable(test-advc-classes)

target_sources(test-advc-classes
PRIVATE
    test_advc_classes.c
    example_class.c
    example_class.h
    child_example_class.c
    child_example_class.h
)

target_link_libraries(test-advc-classes
PUBLIC
    advc-oop
    PkgConfig::CHECK
)

protect_target(test-advc-classes)

add_test(NAME
    test-advc-classes
COMMAND
    test-advc-classes
)

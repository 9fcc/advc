// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2024 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "child_example_class.h"
#include "example_class.h"

#include <check.h>

#include <stdio.h>
#include <stdlib.h>

START_TEST(test_example_object)
{
    errno_t r;

    testing_example_t *example = NULL;
    r = testing_example_new(&example);
    ck_assert_int_eq(r, A_ERR_OK);
    ck_assert(a_object_is_instance_of((a_object_t *) example, a_object_type()));
    ck_assert(a_object_is_instance_of((a_object_t *) example, (a_object_type_t *) testing_example_type()));
    ck_assert(!a_object_is_instance_of((a_object_t *) example, (a_object_type_t *) testing_child_example_type()));

    ck_assert_int_eq(testing_example_get_field1(example), 1);

    ck_assert_int_eq(testing_example_get_field2(example), 2);

    testing_example_set_field1(example, 444);
    ck_assert_int_eq(testing_example_get_field1(example), 444);

    testing_example_set_field2(example, 444);
    ck_assert_int_eq(testing_example_get_field2(example), 444);

    testing_example_ref(example);

    testing_example_unref(example);
    ck_assert_int_eq(testing_example_get_field2(example), 444);

    testing_example_unref(example);
}
END_TEST

START_TEST(test_child_example_object)
{
    errno_t r;

    testing_example_t *example = NULL;
    r = testing_child_example_new((testing_child_example_t **) &example);
    ck_assert_int_eq(r, A_ERR_OK);
    ck_assert(a_object_is_instance_of((a_object_t *) example, a_object_type()));
    ck_assert(a_object_is_instance_of((a_object_t *) example, (a_object_type_t *) testing_example_type()));
    ck_assert(a_object_is_instance_of((a_object_t *) example, (a_object_type_t *) testing_child_example_type()));

    ck_assert_int_eq(testing_example_get_field1(example), 11);

    ck_assert_int_eq(testing_example_get_field2(example), 22);

    testing_example_set_field1(example, 444);
    ck_assert_int_eq(testing_example_get_field1(example), 444);
    ck_assert_int_eq(testing_example_get_field2(example), 0);

    testing_example_set_field2(example, 444);
    ck_assert_int_eq(testing_example_get_field2(example), 444);
    ck_assert_int_eq(testing_example_get_field1(example), 0);

    testing_example_ref(example);

    testing_example_unref(example);
    ck_assert_int_eq(testing_example_get_field2(example), 444);

    testing_example_unref(example);
}
END_TEST


Suite *create_advc_oop_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("advc-oop");

    tc_core = tcase_create("set");

    tcase_add_test(tc_core, test_example_object);
    tcase_add_test(tc_core, test_child_example_object);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(int argc, char *argv[])
{
    Suite *suite = create_advc_oop_suite();
    SRunner *runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);

    int number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    if (number_failed != 0) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2024 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "child_example_class.h"

#include <advc/mem.h>

#include <check.h>

A_IMPLEMENT_OBJECT_TYPE(testing, child_example, TESTING, CHILD_EXAMPLE, testing, example);

errno_t testing_child_example_new(testing_child_example_t **this_out)
{
    errno_t r;

    testing_child_example_t *this = NULL;
    r = a_object_construct((a_object_type_t *) testing_child_example_type(), (a_object_t **) &this);
    a_except(r);

    *this_out = this;
    return A_ERR_OK;
}

#include <stdio.h>

static errno_t initialize_child_example(a_object_t *this_object)
{
    testing_example_t *this = (testing_example_t *) this_object;
    this->field1 = 11;
    this->field2 = 22;

    return A_ERR_OK;
}

static void finalize_child_example(a_object_t *this_object)
{
}

static void set_field1(testing_example_t *this, int new_value)
{
    this->field1 = new_value;
    this->field2 = 0;
}

static void set_field2(testing_example_t *this, int new_value)
{
    this->field1 = 0;
    this->field2 = new_value;
}

static void initialize_child_example_type(a_object_type_t *object_type)
{
    testing_example_type_t *example_type = (testing_example_type_t *) object_type;
    example_type->set_field1 = set_field1;
    example_type->set_field2 = set_field2;
}

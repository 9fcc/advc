// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2024 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef H_ADVC_OOP_TEST_ADVC_CLASSES_H_
#define H_ADVC_OOP_TEST_ADVC_CLASSES_H_

#include <advc/oop.h>

typedef struct {
    a_object_t parent;

    int field1;
    int field2;
} testing_example_t;

typedef struct {
    a_object_type_t parent;

    int (*get_field1)(const testing_example_t *this);
    void (*set_field1)(testing_example_t *this, int new_value);

    int (*get_field2)(const testing_example_t *this);
    void (*set_field2)(testing_example_t *this, int new_value);
} testing_example_type_t;

A_DECLARE_OBJECT_TYPE(testing, example, TESTING, EXAMPLE, a, object);

errno_t testing_example_new(testing_example_t **this_out);

int testing_example_get_field1(const testing_example_t *this);

void testing_example_set_field1(testing_example_t *this, int new_value);

int testing_example_get_field2(const testing_example_t *this);

void testing_example_set_field2(testing_example_t *this, int new_value);

#endif /* H_ADVC_OOP_TEST_ADVC_CLASSES_H_ */

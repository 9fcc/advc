// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2024 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "example_class.h"

#include <advc/mem.h>

#include <check.h>

A_IMPLEMENT_OBJECT_TYPE(testing, example, TESTING, EXAMPLE, a, object);

errno_t testing_example_new(testing_example_t **this_out)
{
    errno_t r;

    testing_example_t *this = NULL;
    r = a_object_construct((a_object_type_t *) testing_example_type(), (a_object_t **) &this);
    a_except(r);

    *this_out = this;
    return A_ERR_OK;
}


int testing_example_get_field1(const testing_example_t *this)
{
    const testing_example_type_t *type = testing_example_get_type(this);
    ck_assert_ptr_nonnull(type);
    ck_assert_ptr_nonnull(type->get_field1);
    return type->get_field1(this);
}

void testing_example_set_field1(testing_example_t *this, int new_value)
{
    const testing_example_type_t *type = testing_example_get_type(this);
    ck_assert_ptr_nonnull(type);
    ck_assert_ptr_nonnull(type->set_field1);
    type->set_field1(this, new_value);
}

int testing_example_get_field2(const testing_example_t *this)
{
    const testing_example_type_t *type = testing_example_get_type(this);
    ck_assert_ptr_nonnull(type);
    ck_assert_ptr_nonnull(type->get_field2);
    return type->get_field2(this);
}

void testing_example_set_field2(testing_example_t *this, int new_value)
{
    const testing_example_type_t *type = testing_example_get_type(this);
    ck_assert_ptr_nonnull(type);
    ck_assert_ptr_nonnull(type->set_field2);
    return type->set_field2(this, new_value);
}


static errno_t initialize_example(a_object_t *this_object)
{
    testing_example_t *this = (testing_example_t *) this_object;
    this->field1 = 1;
    this->field2 = 2;

    return A_ERR_OK;
}

static void finalize_example(a_object_t *this)
{
}

static int get_field1(const testing_example_t *this)
{
    return this->field1;
}

static void set_field1(testing_example_t *this, int new_value)
{
    this->field1 = new_value;
}

static int get_field2(const testing_example_t *this)
{
    return this->field2;
}

static void set_field2(testing_example_t *this, int new_value)
{
    this->field2 = new_value;
}

static void initialize_example_type(a_object_type_t *object_type)
{
    testing_example_type_t *example_type = (testing_example_type_t *) object_type;
    example_type->get_field1 = get_field1;
    example_type->set_field1 = set_field1;
    example_type->get_field2 = get_field2;
    example_type->set_field2 = set_field2;
}

// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#ifndef H_ADVC_TYPES_SYS_H_
#define H_ADVC_TYPES_SYS_H_

/*!
 * @brief File system descriptor.
 *
 * Always use this type for variables that store file descriptor returned by C
 * (and POSIX) standard functions.
 */
typedef int a_filedes_t;

#endif /* H_ADVC_TYPES_SYS_H_ */

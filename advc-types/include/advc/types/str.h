// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#ifndef H_ADVC_STR_STR_H_
#define H_ADVC_STR_STR_H_

#include "str_common.h"

#include <advc/errors/errno.h>

#include <stdbool.h>

/*!
 * @brief Take value of another string.
 *
 * Set internal data pointer and size to the new value data_ptr and size
 * data_size. The function does not allocate memory.
 */
void a_str_take_data(a_str_t *self, char *data_ptr, a_number_t data_size);

/*!
 * @brief Set string value as empty string.
 *
 * Sets string size to zero.
 *
 * @note It is guarantied that data pointer will be set to NULL.
 */
void a_str_set_empty(a_str_t *self);

/*!
 * @brief Set the string value by another string.
 *
 * Copy value of another string (with memory allocation) into self string. The
 * function does not free data, that was previously stored in the string. So it
 * should be used only with uninitialized or finalized variables.
 *
 * Use a_str_fin() to free allocated memory.
 *
 * @returns A_ERR_OK, if operation was successful. Otherwise the error code is
 * returned.
 */
errno_t a_str_set(a_str_t *self, a_str_t *other);

/*!
 * @brief Finalize the string.
 *
 * Free data memory. The function does not free its internal data, so it can be
 * used only with variables that were not dynamically allocated.
 *
 * @returns Size of the string.
 */
void a_str_fin(a_str_t *self);

/*!
 * @brief Get pointer to the data of the string.
 *
 * Data is non-null-terminated string. Data can represent a slice of another
 * string.
 *
 * @returns Size of the string.
 */
const char *a_str_get_data(const a_str_t *self);

/*!
 * @brief Get size of the string.
 *
 * @returns Size of the string.
 */
a_number_t a_str_get_size(const a_str_t *self);

/*!
 * @brief Compare two strings.
 *
 * @retval 0 the strings are equal.
 * @retval negative number, if self if lesser than other.
 * @retval positive number, if self if greater than other.
 */
int a_str_cmp(const a_str_t *self, const a_str_t *other);

/*!
 * @brief Are the string equal?
 *
 * Empty string are equal, too.
 *
 * @returns true, if the strings are equal.
 */
static inline bool a_str_equal(const a_str_t *self, const a_str_t *other)
{
    if (a_str_cmp(self, other) == 0) {
        return true;
    }
    return false;
}

/*!
 * @brief Is the string empty?
 *
 * Empty string is the string that has zero size. The pointer to the data in
 * empty string may be NULL or non-NULL.
 *
 * @returns true, if size of the string is zero.
 */
static inline bool a_str_is_empty(const a_str_t *self)
{
    if (self->size == 0) {
        return true;
    }
    return false;
}

/*!
 * @brief Whether the string ends with another string.
 *
 * If both string are empty the function return true. If ending string is empty
 * the function returns true.
 *
 * @param[in] self Checked string.
 * @param[in] ending Ending string.
 *
 * @returns true, if self string ends with ending string.
 */
bool a_str_ends_with(const a_str_t *self, const a_str_t *ending);

/*!
 * @brief Whether the string starts with another string.
 *
 * If both string are empty the function return true. If starting string is
 * empty the function returns true.
 *
 * @param[in] self Checked string.
 * @param[in] starting Starting string.
 *
 * @returns true, if self string starts with ending string.
 */
bool a_str_starts_with(const a_str_t *self, const a_str_t *starting);

/*!
 * @brief Get a slice of the string.
 *
 * The function returns A_ERR_BUF_OVERFLOW if substring is out the the string size..
 *
 * @param[in] self The string.
 * @param[in] offset Position of substring.
 * @param[in] size Size of substring.
 * @param[out] slice_out Pointer to substring, which will store returned slice.
 *
 * @returns A_ERR_OK, if operation was successful. Otherwise the error code is
 * returned.
 */
errno_t a_str_get_slice(const a_str_t *self,
                        a_number_t offset,
                        a_number_t size,
                        a_str_t *slice_out);

#define A_STR_BY_LITERAL(str_data) (a_str_t)\
    {\
        .data = str_data,\
        .size = sizeof(str_data),\
    }

#define A_STR_P_BY_LITERAL(str_data) &(a_str_t) {\
        .data = str_data,\
        .size = sizeof(str_data),\
    }

/*!
 * @brief Get string initializer by data and its size.
 *
 * This macro can be used with global variables.
 *
 * @returns a_str_t struct.
 */
#define A_STR_BY_DATA(str_data, data_size)                                     \
    (a_str_t)                                                                  \
    {                                                                          \
        .data = str_data, .size = data_size                                    \
    }

/*!
 * @brief Get string initializer for the pointer by data and its size.
 *
 * This macro can be used with global variables.
 *
 * @returns a_str_t * struct.
 */
#define A_STR_P_BY_DATA(str_data, data_size)                                   \
    &(a_str_t)                                                                 \
    {                                                                          \
        .data = str_data, .size = data_size                                    \
    }

/*!
 * @brief Get arguments for the format qualifier %.*s of printf-like functions.
 *
 * @returns Two values, separated by comma.
 */
#define A_STR_F(str) ((int) (str)->size), (str)->data

#endif /* H_ADVC_STR_STR_H_ */

// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#ifndef H_ADVC_STR_STR_COMMON_H_
#define H_ADVC_STR_STR_COMMON_H_

#include "int.h"

/*!
 * Sized string type.
 *
 * Can be used to store static strings, dynamically allocated strings, or string
 * slices.
 */
typedef struct {
    //! Pointer to string data
    char *data;

    //! Size of the string
    a_number_t size;
} a_str_t;

#endif /* H_ADVC_STR_STR_COMMON_H_ */

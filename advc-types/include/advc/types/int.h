#ifndef H_ADVC_TYPES_INT_H_
#define H_ADVC_TYPES_INT_H_

#include <stddef.h>
#include <stdint.h>
#include <limits.h>

/*
 * Integer type usage examples:
 * * a_size_t is for the size of a memory allocation;
 *       ↓
 * * a_number_t is for a large array length or an objects count;
 *       ↓
 * * a_many_t is for a command line arguments count;
 *       ↓
 * * a_several_t is for a very small number of something.
 */

/*!
 * @brief Size of memory region.
 *
 * Use this type for values that limited by memory size.
 */
typedef size_t a_size_t;

//! Minimum value that can be stored in a_size_t
#define A_SIZE_MIN SIZE_MIN

//! Maximum value that can be stored in a_size_t
#define A_SIZE_MAX SIZE_MAX

/*!
 * @brief Count of objects, length of something, or an identifier.
 *
 * Use this type to store the count of object, the array length or the string length,
 * if a possible maximum count is limited by 32 bits. It also can be used to the
 * identifiers.
 *
 * It is not intended to be used as an unbounded size of a memory region.
 */
typedef uint_fast32_t a_number_t;

//! Minimum value that can be stored in a_number_t
#define A_NUMBER_MIN UINT_FAST32_MIN

//! Maximum value that can be stored in a_number_t
#define A_NUMBER_MAX UINT_FAST32_MAX

// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

/*!
 * @brief Not so big count of objects or length.
 *
 * Use this type to store count of object, array length or string length,
 * if a maximum possible count is limited to 16 bits.
 *
 * It is not intended to be used as an unbounded size of a memory region.
 */
typedef uint_fast16_t a_many_t;

//! Minimum value that can be stored in a_many_t
#define A_MANY_MIN UINT_FAST16_MIN

//! Maximum value that can be stored in a_many_t
#define A_MANY_MAX UINT_FAST16_MAX

/*!
 * @brief Small count of objects or small length.
 *
 * Use this type to store count of object, array length or string length,
 * if a maximum possible count is limited to 8 bits.
 *
 * It is not intended to be used as an unbounded size of a memory region.
 */
typedef uint_fast8_t a_several_t;

//! Minimum value that can be stored in a_several_t
#define A_SEVERAL_MIN UINT_FAST8_MIN

//! Maximum value that can be stored in a_several_t
#define A_SEVERAL_MAX UINT_FAST8_MAX

#endif /* H_ADVC_TYPES_INT_H_ */

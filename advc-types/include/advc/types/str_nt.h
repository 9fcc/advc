#ifndef H_ADVC_TYPES_STR_NT_H_
#define H_ADVC_TYPES_STR_NT_H_
// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#include <advc/errors/errno.h>

#include "str_common.h"

/*!
 * @brief Get size of null-terminated string.
 *
 * This function doesn't support UTF-8 and returns the size in bytes.
 *
 * @returns size of the string without terminating null character.
 */
a_number_t a_ntstr_size(const char *ntstr);

/*!
 * @brief Copy string contents to newly-allocated null-terminated string.
 *
 * Copies string contents and returns new newly-allocated string via ntstr_ptr
 * pointer.
 *
 * Use a_mem_free() to free allocated memory.
 *
 * @returns A_ERR_OK, if operation was successful. Otherwise the error code is
 * returned.
 */
errno_t a_str_to_ntstr(const a_str_t *self, char **ntstr_ptr);

/*!
 * @brief Set new value of string by null-terminated string.
 *
 * Copies null-terminated string (with memory allocation) and sets internal data
 * to new value. The function does not free its internal data, so it can be
 * used only with variables that were not dynamically allocated.
 *
 * Use a_str_fin() to free allocated memory.
 *
 * @returns A_ERR_OK, if operation was successful. Otherwise the error code is
 * returned.
 */
errno_t a_str_set_ntstr(a_str_t *self, const char *ntstr);

/*!
 * @brief Get string initializer by null-terminated string.
 *
 * This macro should not be used with global variables.
 *
 * @returns a_str_t struct.
 */
#define A_STR(ntstr)                                                           \
    (a_str_t)                                                                  \
    {                                                                          \
        .data = ntstr, .size = a_ntstr_size(ntstr)                             \
    }

/*!
 * @brief Get string pointer initializer by null-terminated string.
 *
 * This macro should not be used with global variables.
 *
 * @returns a_str_t * struct.
 */
#define A_STR_P(ntstr)                                                         \
    &(a_str_t)                                                                 \
    {                                                                          \
        .data = (char *) ntstr, .size = a_ntstr_size(ntstr)                    \
    }

/*!
 * @brief Builtin empty string.
 *
 * This variable can be used to pass empty string to function arguments.
 */
extern a_str_t *a_str_null;

#endif /* H_ADVC_TYPES_STR_NT_H_ */

// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#include <advc/types/str_nt.h>

#include <advc/mem.h>

#include <string.h>

a_str_t *a_str_null = &(a_str_t){.data = NULL, .size = 0};

a_number_t a_ntstr_size(const char *ntstr)
{
    return strlen(ntstr);
}

errno_t a_str_to_ntstr(const a_str_t *self, char **ntstr_ptr)
{
    errno_t e;

    char *ntstr;
    e = a_mem_alloc(&ntstr, self->size + 1);
    if (e) {
        return e;
    }

    memcpy(ntstr, self->data, self->size);
    ntstr[self->size] = '\0';

    *ntstr_ptr = ntstr;
    return A_ERR_OK;
}

errno_t a_str_set_ntstr(a_str_t *self, const char *ntstr)
{
    errno_t e;

    a_number_t size = strlen(ntstr);

    void *data;
    e = a_mem_alloc(&data, size);
    if (e) {
        return e;
    }

    memcpy(data, ntstr, size);

    self->data = data;
    self->size = size;
    return A_ERR_OK;
}

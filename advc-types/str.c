// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#include <advc/mem.h>
#include <advc/types/str.h>
#include <string.h>

void a_str_take_data(a_str_t *self, char *data, a_number_t data_size)
{
    self->data = data;
    self->size = data_size;
}

void a_str_set_empty(a_str_t *self)
{
    self->data = NULL;
    self->size = 0;
}

errno_t a_str_set(a_str_t *self, a_str_t *other)
{
    errno_t e;

    void *data;
    e = a_mem_alloc(&data, other->size);
    if (e) {
        return e;
    }
    memcpy(data, other->data, other->size);

    self->data = data;
    self->size = other->size;
    return A_ERR_OK;
}

void a_str_fin(a_str_t *self)
{
    a_mem_free(self->data);
}

const char *a_str_get_data(const a_str_t *self)
{
    return self->data;
}

a_number_t a_str_get_size(const a_str_t *self)
{
    return self->size;
}

int a_str_cmp(const a_str_t *self, const a_str_t *other)
{
    if ((self->size == 0) && (other->size == 0)) {
        return 0;
    }

    char *data1 = self->data;
    char *data2 = other->data;

    a_number_t common_size = self->size < other->size ? self->size : other->size;
    for (a_number_t i = 0; i < common_size; ++i) {
        if (data1[i] > data2[i]) {
            return 1;
        } else if (data1[i] < data2[i]) {
            return -1;
        }
    }

    if (self->size > other->size) {
        return 1;
    } else if (self->size == other->size) {
        return 0;
    } else {
        return -1;
    }

    return 0;
}

bool a_str_ends_with(const a_str_t *self, const a_str_t *ending)
{
    if (self->size < ending->size) {
        return false;
    }

    const char *ptr = self->data + self->size;

    const char *ending_start = ending->data;
    const char *ending_ptr = ending_start + ending->size;
    while (ending_ptr != ending_start) {
        --ending_ptr;
        --ptr;

        if (*ptr != *ending_ptr) {
            return false;
        }
    }

    return true;
}

bool a_str_starts_with(const a_str_t *self, const a_str_t *starting)
{
    if (self->size < starting->size) {
        return false;
    }

    const char *ptr = self->data;

    const char *starting_end = starting->data + starting->size;
    const char *starting_ptr = starting->data;
    while (starting_ptr != starting_end) {
        if (*ptr != *starting_ptr) {
            return false;
        }

        ++starting_ptr;
        ++ptr;
    }

    return true;
}

errno_t a_str_get_slice(const a_str_t *self,
                        a_number_t offset,
                        a_number_t size,
                        a_str_t *slice_out)
{
    if (offset + size > self->size) {
        return A_ERR_BUF_OVERFLOW;
    }

    slice_out->data = self->data + offset;
    slice_out->size = size;
    return A_ERR_OK;
}

// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023–2024 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <advc/types.h>

#include <check.h>

#include <stdio.h>
#include <stdlib.h>

START_TEST(test_str_take_data)
{
    char *test_str = "text";
    size_t text_str_size = strlen(test_str);

    a_str_t str;
    a_str_take_data(&str, test_str, strlen(test_str));

    a_number_t size = a_str_get_size(&str);
    ck_assert_uint_eq(size, text_str_size);

    const char *data = a_str_get_data(&str);
    ck_assert_mem_eq(data, test_str, text_str_size);
}
END_TEST

START_TEST(test_str_set_empty)
{
    a_str_t str = A_STR("test");
    a_str_set_empty(&str);
    ck_assert(a_str_is_empty(&str));
    ck_assert_uint_eq(a_str_get_size(&str), 0);
}
END_TEST

START_TEST(test_str_set)
{
    errno_t e;

    char *test_str = "text";
    size_t text_str_size = strlen(test_str);

    a_str_t str1;
    a_str_take_data(&str1, test_str, strlen(test_str));

    a_str_t str2;
    e = a_str_set(&str2, &str1);
    ck_assert_int_eq(e, A_ERR_OK);

    a_number_t size = a_str_get_size(&str2);
    ck_assert_uint_eq(size, text_str_size);

    const char *data = a_str_get_data(&str2);
    ck_assert_mem_eq(data, test_str, text_str_size);

    a_str_fin(&str2);
}
END_TEST

START_TEST(test_str_set_ntstr)
{
    errno_t e;

    char *test_str = "text";
    size_t text_str_size = strlen(test_str);

    a_str_t str;
    e = a_str_set_ntstr(&str, test_str);
    ck_assert_int_eq(e, A_ERR_OK);

    a_number_t size = a_str_get_size(&str);
    ck_assert_uint_eq(size, text_str_size);

    const char *data = a_str_get_data(&str);
    ck_assert_mem_eq(data, test_str, text_str_size);

    a_str_fin(&str);
}
END_TEST

START_TEST(test_printf)
{
    char *test_str = "text";
    size_t text_str_size = strlen(test_str);

    a_str_t str;
    a_str_take_data(&str, test_str, text_str_size);

    char buf[128];
    int n = snprintf(buf, sizeof(buf), "%.*s", A_STR_F(&str));
    ck_assert_int_eq(n, text_str_size);

    ck_assert_str_eq(buf, test_str);
}
END_TEST

START_TEST(test_str_ends_with)
{
    a_str_t *str1 = A_STR_P("Test string.");

    ck_assert(a_str_ends_with(str1, A_STR_P("string.")));
    ck_assert(!a_str_ends_with(str1, A_STR_P("string")));

    ck_assert(!a_str_ends_with(a_str_null, A_STR_P("string")));
    ck_assert(a_str_ends_with(str1, a_str_null));
    ck_assert(a_str_ends_with(a_str_null, a_str_null));
}
END_TEST

START_TEST(test_str_starts_with)
{
    a_str_t *str1 = A_STR_P("Test string.");

    ck_assert(a_str_starts_with(str1, A_STR_P("Test")));
    ck_assert(!a_str_starts_with(str1, A_STR_P("string.")));

    ck_assert(!a_str_starts_with(a_str_null, A_STR_P("est")));
    ck_assert(a_str_starts_with(str1, a_str_null));
    ck_assert(a_str_starts_with(a_str_null, a_str_null));
}
END_TEST

START_TEST(test_str_cmp)
{
    a_str_t *str1 = A_STR_P("Test string.");
    a_str_t *str2 = A_STR_P("Another test string.");

    ck_assert(a_str_cmp(str1, str2) > 0);
    ck_assert(a_str_cmp(str2, str1) < 0);
    ck_assert(a_str_cmp(str1, str1) == 0);
    ck_assert(a_str_cmp(a_str_null, a_str_null) == 0);

    a_str_t *str3 = A_STR_P("arg2");
    a_str_t *str4 = A_STR_P("arg3");
    ck_assert(a_str_cmp(str3, str4) < 0);
    ck_assert(a_str_cmp(str4, str3) > 0);
}
END_TEST

START_TEST(test_str_get_slice)
{
    a_str_t *str1 = A_STR_P("Test string.");

    errno_t e;

    a_str_t str_slice;
    e = a_str_get_slice(str1, 5, 6, &str_slice);
    ck_assert_int_eq(e, A_ERR_OK);

    ck_assert(a_str_equal(&str_slice, A_STR_P("string")));

    e = a_str_get_slice(str1, 0, 0, &str_slice);
    ck_assert_int_eq(e, A_ERR_OK);

    ck_assert(a_str_is_empty(&str_slice));

    e = a_str_get_slice(str1, 8, 1000, &str_slice);
    ck_assert_int_eq(e, A_ERR_BUF_OVERFLOW);
}
END_TEST

Suite *create_advc_types_str_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("advc-types-str");

    tc_core = tcase_create("set");

    tcase_add_test(tc_core, test_str_take_data);
    tcase_add_test(tc_core, test_str_set);
    tcase_add_test(tc_core, test_str_set_ntstr);
    tcase_add_test(tc_core, test_printf);
    tcase_add_test(tc_core, test_str_ends_with);
    tcase_add_test(tc_core, test_str_starts_with);
    tcase_add_test(tc_core, test_str_set_empty);
    tcase_add_test(tc_core, test_str_cmp);
    tcase_add_test(tc_core, test_str_get_slice);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(int argc, char *argv[])
{
    Suite *suite = create_advc_types_str_suite();
    SRunner *runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);

    int number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    if (number_failed != 0) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

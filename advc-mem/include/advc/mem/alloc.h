// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#ifndef H_ADVC_ALLOC_H_
#define H_ADVC_ALLOC_H_

#include <advc/errors/errno.h>

#include <stddef.h>

errno_t a_mem_alloc_voidptr(void **mem_ptr, size_t requested_size);

errno_t a_mem_realloc_voidptr(void **mem_ptr, size_t requested_size);

void a_mem_free(void *mem);

#define a_mem_alloc(mem_ptr, requested_size)                                   \
    a_mem_alloc_voidptr((void **) (mem_ptr), (requested_size))

#define a_mem_realloc(mem_ptr, requested_size)                                 \
    a_mem_realloc_voidptr((void **) (mem_ptr), (requested_size))

#define a_mem_alloc_array(mem_out, count)                                   \
    a_mem_alloc_voidptr((void **) (mem_out), sizeof(**(mem_out)) * (count))

#define a_mem_realloc_array(mem_out, count)                                   \
    a_mem_realloc_voidptr((void **) (mem_out), sizeof(**(mem_out)) * (count))

#define a_mem_new(mem_out)\
    a_mem_alloc_voidptr((void **) (mem_out), sizeof(*(mem_out)))

#endif /* H_ADVC_ALLOC_H_ */

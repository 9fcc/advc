// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#include <advc/mem/alloc.h>

#include <stdlib.h>

errno_t a_mem_alloc_voidptr(void **mem_ptr, size_t requested_size)
{
    void *mem = malloc(requested_size);
    if (!mem) {
        return errno;
    }

    *mem_ptr = mem;
    return A_ERR_OK;
}

errno_t a_mem_realloc_voidptr(void **mem_ptr, size_t requested_size)
{
    void *mem = realloc(*mem_ptr, requested_size);
    if (!mem) {
        return errno;
    }

    *mem_ptr = mem;
    return A_ERR_OK;
}

void a_mem_free(void *mem)
{
    free(mem);
}

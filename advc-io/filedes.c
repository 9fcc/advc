// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#include <advc/io.h>
#include <advc/mem.h>

#include <fcntl.h>
#include <grp.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

errno_t a_fd_get_size(a_filedes_t fd, a_size_t *size_ptr)
{
    struct stat statbuf;
    int res = fstat(fd, &statbuf);
    if (res) {
        return errno;
    }

    *size_ptr = statbuf.st_size;
    return A_ERR_OK;
}

errno_t a_fd_get_common_info(a_filedes_t fd, a_filedes_common_info_t *info_ptr)
{
    struct stat statbuf;
    int res = fstat(fd, &statbuf);
    if (res) {
        return errno;
    }

    long buffer_size = sysconf(_SC_GETPW_R_SIZE_MAX);
    if (buffer_size == -1) {
        return errno;
    }

    errno_t e;

    void *buffer;
    e = a_mem_alloc(&buffer, buffer_size);
    if (e) {
        return e;
    }

    struct passwd pwd_buffer;
    struct passwd *pwd;
    e = getpwuid_r(statbuf.st_uid, &pwd_buffer, buffer, buffer_size, &pwd);
    if (e) {
        goto undo_buffer;
    }
    char *owner_user = NULL;
    size_t owner_user_size = 0;
    if (pwd) {
        owner_user_size = strlen(pwd->pw_name);
        e = a_mem_alloc(&owner_user, owner_user_size);
        if (e) {
            goto undo_buffer;
        }
        memcpy(owner_user, pwd->pw_name, owner_user_size);
    }

    struct group grp_buffer;
    struct group *grp;
    e = getgrgid_r(statbuf.st_gid, &grp_buffer, buffer, buffer_size, &grp);
    if (e) {
        goto undo_owner_user;
    }
    char *owner_group = NULL;
    a_size_t owner_group_size = 0;
    if (grp) {
        owner_group_size = strlen(grp->gr_name);
        e = a_mem_alloc(&owner_group, owner_group_size);
        if (e) {
            goto undo_owner_user;
        }
        memcpy(owner_group, grp->gr_name, owner_group_size);
    }

    info_ptr->links_count = statbuf.st_nlink;
    info_ptr->size = statbuf.st_size;
    info_ptr->is_dir = S_ISDIR(statbuf.st_mode);
    info_ptr->owner_read = (statbuf.st_mode & S_IRUSR);
    info_ptr->owner_write = (statbuf.st_mode & S_IWUSR);
    info_ptr->owner_execute = (statbuf.st_mode & S_IXUSR);
    info_ptr->group_read = (statbuf.st_mode & S_IRGRP);
    info_ptr->group_write = (statbuf.st_mode & S_IWGRP);
    info_ptr->group_execute = (statbuf.st_mode & S_IXGRP);
    info_ptr->other_read = (statbuf.st_mode & S_IROTH);
    info_ptr->other_write = (statbuf.st_mode & S_IWOTH);
    info_ptr->other_execute = (statbuf.st_mode & S_IXOTH);
    info_ptr->modified_timestamp = statbuf.st_mtime;
    info_ptr->owner_user = owner_user;
    info_ptr->owner_user_size = owner_user_size;
    info_ptr->owner_group = owner_group;
    info_ptr->owner_group_size = owner_group_size;

    a_mem_free(buffer);
    return A_ERR_OK;

undo_owner_user:
    a_mem_free(owner_user);
undo_buffer:
    a_mem_free(buffer);
    return e;
}

void a_fd_finalize_common_info(a_filedes_common_info_t *info)
{
    a_mem_free(info->owner_user);
    a_mem_free(info->owner_group);
}

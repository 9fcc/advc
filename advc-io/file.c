// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2023 Andrey N. Dotsenko
 */

#include <advc/io/file.h>
#include <advc/types.h>

#include <dirent.h>
#include <fcntl.h>
#include <stddef.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

errno_t a_file_open_path(const a_str_t *path, a_filedes_t *fd_ptr)
{
    errno_t e;

    char *path_ntstr;
    e = a_str_to_ntstr(path, &path_ntstr);
    if (e) {
        return e;
    }

    a_filedes_t fd = open(path_ntstr, O_PATH);
    if (fd == -1) {
        e = errno;
        goto undo_path_ntstr;
    }

    a_mem_free(path_ntstr);

    *fd_ptr = fd;
    return A_ERR_OK;

undo_path_ntstr:
    a_mem_free(path_ntstr);
    return e;
}

errno_t a_file_is_dir(const a_str_t *path, bool *is_dir_ptr)
{
    errno_t e;

    char *path_ntstr;
    e = a_str_to_ntstr(path, &path_ntstr);
    if (e) {
        return e;
    }

    struct stat statbuf;
    int res = stat(path_ntstr, &statbuf);
    if (res) {
        e = errno;
        goto undo_path_ntstr;
    }

    a_mem_free(path_ntstr);
    *is_dir_ptr = S_ISDIR(statbuf.st_mode);
    return A_ERR_OK;

undo_path_ntstr:
    a_mem_free(path_ntstr);
    return e;
}

errno_t a_dir_filter(const a_str_t *dir_path,
                     a_dir_filter_callback_t cb_func,
                     void *user_data,
                     a_compare_nt_str_t sort_func,
                     a_str_t **files_ptr,
                     a_number_t *files_count_ptr)
{

    errno_t e;

    char *dir_path_ntstr;
    e = a_str_to_ntstr(dir_path, &dir_path_ntstr);
    if (e) {
        return e;
    }

    DIR *ds = opendir(dir_path_ntstr);
    if (!ds) {
        e = errno;
        goto undo_dir_path_ntstr;
    }
    a_filedes_t fd = dirfd(ds);

    a_str_t *files = NULL;
    a_number_t files_length = 10;
    e = a_mem_alloc(&files, files_length * sizeof(*files));
    if (e) {
        goto undo_ds;
    }

    int i = 0;
    for (struct dirent *dir = readdir(ds); dir != NULL; dir = readdir(ds)) {
        if (cb_func) {
            bool accept = true;
            e = cb_func(dir_path, fd, A_STR_P(dir->d_name), user_data, &accept);
            if (e) {
                return e;
            }
            if (!accept) {
                continue;
            }
        }
        if (i >= files_length) {
            size_t new_length = files_length << 1;
            e = a_mem_realloc(&files, new_length * sizeof(*files));
            if (e) {
                new_length = files_length + 1;
                e = a_mem_realloc(&files, new_length * sizeof(*files));
            }
            if (e) {
                goto undo_files;
            }
            files_length = new_length;
        }

        a_str_t name_copy;
        e = a_str_set_ntstr(&name_copy, dir->d_name);
        if (e) {
            goto undo_files;
        }

        if (sort_func && (i > 0)) {
            int j;
            for (j = 0; j < i; ++j) {
                a_str_t *curr_file = &files[j];
                if (sort_func(&name_copy, curr_file) < 0) {
                    memmove(&files[j + 1], &files[j], sizeof(*files) * (i - j));
                    break;
                }
            }
            files[j] = name_copy;
        } else {
            files[i] = name_copy;
        }
        ++i;
    }

    closedir(ds);

    a_mem_free(dir_path_ntstr);

    *files_ptr = files;
    *files_count_ptr = i;
    return A_ERR_OK;

undo_files:
    for (--i; i >= 0; --i) {
        a_str_fin(&files[i]);
    }
    a_mem_free(files);
undo_ds:
    closedir(ds);
undo_dir_path_ntstr:
    a_mem_free(dir_path_ntstr);
    return e;
}

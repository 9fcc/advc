// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */
#ifndef H_ADVC_FD_H_
#define H_ADVC_FD_H_

#include <advc/types.h>

#include <stdbool.h>
#include <stddef.h> // for size_t
#include <stdint.h>

typedef struct {
    a_size_t size;

    char *owner_user;
    a_size_t owner_user_size;

    char *owner_group;
    a_size_t owner_group_size;

    int_fast64_t modified_timestamp;

    uint_fast64_t links_count;

    bool is_dir : 1;

    bool owner_read : 1;
    bool owner_write : 1;
    bool owner_execute : 1;

    bool group_read : 1;
    bool group_write : 1;
    bool group_execute : 1;

    bool other_read : 1;
    bool other_write : 1;
    bool other_execute : 1;
} a_filedes_common_info_t;

errno_t a_fd_get_size(a_filedes_t fd, a_size_t *size_ptr);

errno_t a_fd_get_common_info(a_filedes_t fd, a_filedes_common_info_t *info_ptr);

void a_fd_finalize_common_info(a_filedes_common_info_t *info);

#endif /* H_ADVC_FD_H_ */

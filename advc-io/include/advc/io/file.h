// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */
#ifndef H_ADVC_FILE_H_
#define H_ADVC_FILE_H_

#include <advc/mem.h>
#include <advc/types.h>

#include <stdbool.h>

errno_t a_file_open_path(const a_str_t *path, a_filedes_t *fd_ptr);

errno_t a_file_is_dir(const a_str_t *path, bool *is_directory_ptr);

typedef errno_t (*a_dir_filter_callback_t)(const a_str_t *dir_path,
                                           a_filedes_t dir_fd,
                                           const a_str_t *filename,
                                           void *user_data,
                                           bool *accept_ptr);
typedef int (*a_compare_nt_str_t)(const a_str_t *name1, const a_str_t *name2);

errno_t a_dir_filter(const a_str_t *dir_path,
                     a_dir_filter_callback_t cb_func,
                     void *user_data,
                     a_compare_nt_str_t sort_func,
                     a_str_t **files_ptr,
                     a_number_t *files_count_ptr);

#endif /* H_ADVC_FILE_H_ */

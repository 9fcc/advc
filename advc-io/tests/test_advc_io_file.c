// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <advc/io.h>

#include <check.h>

#include <stdlib.h>

START_TEST(test_file_is_dir)
{
    errno_t e;

    bool is_directory = false;
    e = a_file_is_dir(A_STR_P("."), &is_directory);
    ck_assert_msg(e == A_ERR_OK, "%s", strerror(e));
    ck_assert(is_directory);

    e = a_file_is_dir(A_STR_P("/non-existent-directory"), &is_directory);
    ck_assert_int_ne(e, A_ERR_OK);

    e = a_file_is_dir(A_STR_P("./test-advc-io-file"), &is_directory);
    ck_assert_msg(e == A_ERR_OK, "%s", strerror(e));
    ck_assert(!is_directory);
}
END_TEST

#include <stdio.h>

static errno_t filter_cb(const a_str_t *dir_path,
                         a_filedes_t dir_fd,
                         const a_str_t *filename,
                         void *user_data,
                         bool *accept_ptr)
{
    const char *filename_ntstr = a_str_get_data(filename);
    if ((filename_ntstr != NULL) && (filename_ntstr[0] == '.')) {
        *accept_ptr = false;
        return A_ERR_OK;
    }

    const a_str_t *files = user_data;
    for (const a_str_t *curr_file = &files[0]; !a_str_is_empty(curr_file);
         ++curr_file) {
        fprintf(stderr,
                "%.*s %.*s\n",
                (int) curr_file->size,
                curr_file->data,
                (int) filename->size,
                filename->data);
        if (a_str_equal(curr_file, filename)) {
            return A_ERR_OK;
        }
    }

    return A_ERR_NOT_FOUND;
}

START_TEST(test_dir_filter)
{
    errno_t e;

    const a_str_t expected_files[] = {
            A_STR("1.txt"),
            A_STR("2.txt"),
            A_STR("3.bin"),
            A_STR(""),
    };

    a_str_t *files = NULL;
    size_t files_count = 0;
    e = a_dir_filter(A_STR_P("./test_dir"),
                     filter_cb,
                     (void *) expected_files,
                     a_str_cmp,
                     &files,
                     &files_count);
    ck_assert_int_eq(e, A_ERR_OK);

    ck_assert_uint_eq(files_count, 3);

    for (int i = 0; i < files_count; ++i) {
        ck_assert_msg(a_str_equal(&files[i], &expected_files[i]),
                      "Unknown file No %d: %.*s; expected: %.*s\n",
                      i,
                      A_STR_F(&files[i]),
                      A_STR_F(&expected_files[i]));
    }

    for (int i = 0; i < files_count; ++i) {
        a_str_fin(&files[i]);
    }
    a_mem_free(files);
}
END_TEST

Suite *create_advc_io_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("advc-io");

    tc_core = tcase_create("file");

    tcase_add_test(tc_core, test_file_is_dir);
    tcase_add_test(tc_core, test_dir_filter);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(int argc, char *argv[])
{
    Suite *suite = create_advc_io_suite();
    SRunner *runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);

    int number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    if (number_failed != 0) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

#ifndef H_ADVC_ARGS_H_
#define H_ADVC_ARGS_H_

#include <advc/types.h>

typedef enum {
    A_CMDARG_VALUE_ABSENT,
    A_CMDARG_VALUE_OPTIONAL,
    A_CMDARG_VALUE_REQUIRED,
} a_cmdarg_value_presence_t;

typedef struct {
    a_str_t long_name;
    a_str_t short_name;
    a_cmdarg_value_presence_t value_presence;
    a_str_t default_value;
    a_str_t *value_out;
    bool *is_found_out;
} a_cmdarg_t;

errno_t a_cmdargs_parse(char *argv[],
                        a_many_t argc,
                        a_cmdarg_t cmdargs[],
                        a_many_t cmdargs_count);

#endif /* H_ADVC_ARGS_H_ */

#include <advc/cmdargs.h>

static a_cmdarg_t *get_arg_by_short_name(const a_str_t *short_name,
                                         a_cmdarg_t cmdargs[],
                                         a_many_t cmdargs_count)
{
    errno_t e;

    a_str_t name;
    e = a_str_get_slice(short_name, 1, a_str_get_size(short_name) - 1, &name);
    if (e) {
        return NULL;
    }

    for (a_many_t i = 0; i < cmdargs_count; ++i) {
        a_cmdarg_t *arg = &cmdargs[i];
        if (a_str_equal(&name, &arg->short_name)) {
            return arg;
        }
    }
    return NULL;
}

static a_cmdarg_t *get_arg_by_long_name(const a_str_t *long_name,
                                        a_cmdarg_t cmdargs[],
                                        a_many_t cmdargs_count)
{
    a_str_t name;
    a_str_get_slice(long_name, 2, a_str_get_size(long_name) - 2, &name);
    for (a_many_t i = 0; i < cmdargs_count; ++i) {
        a_cmdarg_t *arg = &cmdargs[i];
        if (a_str_equal(&name, &arg->long_name)) {
            return arg;
        }
    }
    return NULL;
}

static void prepare_cmdargs(a_cmdarg_t cmdargs[], a_many_t cmdargs_count)
{
    for (a_many_t i = 0; i < cmdargs_count; ++i) {
        a_cmdarg_t *arg = &cmdargs[i];
        if (arg->value_out) {
            a_str_set_empty(arg->value_out);
        }
        if (arg->is_found_out) {
            *arg->is_found_out = false;
        }
    }
}

static void clear_cmdargs(a_cmdarg_t cmdargs[], a_many_t cmdargs_count)
{
    for (a_many_t i = 0; i < cmdargs_count; ++i) {
        a_cmdarg_t *arg = &cmdargs[i];
        a_str_fin(arg->value_out);
    }
}

errno_t a_cmdargs_parse(char *argv[],
                        a_many_t argc,
                        a_cmdarg_t cmdargs[],
                        a_many_t cmdargs_count)
{
    errno_t e;

    prepare_cmdargs(cmdargs, cmdargs_count);

    bool arg_changed = false;
    a_cmdarg_t *curr_arg = NULL;

    for (a_many_t i = 0; i < argc; ++i) {
        a_str_t arg_str = A_STR(argv[i]);

        if (a_str_starts_with(&arg_str, A_STR_P("--"))) {
            curr_arg = get_arg_by_long_name(&arg_str, cmdargs, cmdargs_count);
            arg_changed = true;
        } else if (a_str_starts_with(&arg_str, A_STR_P("-"))) {
            curr_arg = get_arg_by_short_name(&arg_str, cmdargs, cmdargs_count);
            arg_changed = true;
        }

        if (arg_changed) {
            if (curr_arg && curr_arg->is_found_out) {
                *curr_arg->is_found_out = true;
            }
            arg_changed = false;
            continue;
        }

        if (curr_arg == NULL) {
            continue;
        }

        if (curr_arg->value_presence != A_CMDARG_VALUE_ABSENT) {
            if (curr_arg->value_out) {
                a_str_fin(curr_arg->value_out);
                e = a_str_set(curr_arg->value_out, &arg_str);
                if (e) {
                    goto undo_cmdargs;
                }
            }
        }
    }
    return A_ERR_OK;

undo_cmdargs:
    clear_cmdargs(cmdargs, cmdargs_count);
    return e;
}

#include <advc/cmdargs.h>

#include <check.h>

#include <stdio.h>
#include <stdlib.h>

START_TEST(test_cmdargs_parse)
{
    char *argv[] = {"--flag1", "--arg2", "value2", "-a", "value3", "-b"};
    int argc = sizeof(argv) / sizeof(argv[1]);

    errno_t e;

    a_str_t arg1 = {};
    bool arg1_found = false;

    a_str_t arg2 = {};
    bool arg2_found = false;

    a_str_t arg3 = {};
    bool arg3_found = false;

    bool arg4_found = false;
    bool arg5_found = false;

    a_cmdarg_t cmdargs[] = {
            (a_cmdarg_t){
                    .long_name = A_STR("flag1"),
                    .is_found_out = &arg1_found,
                    .value_presence = A_CMDARG_VALUE_ABSENT,
                    .value_out = &arg1,
            },
            (a_cmdarg_t){
                    .long_name = A_STR("arg2"),
                    .is_found_out = &arg2_found,
                    .value_presence = A_CMDARG_VALUE_REQUIRED,
                    .value_out = &arg2,
            },
            (a_cmdarg_t){
                    .short_name = A_STR("a"),
                    .long_name = A_STR("arg3"),
                    .is_found_out = &arg3_found,
                    .value_presence = A_CMDARG_VALUE_REQUIRED,
                    .value_out = &arg3,
            },
            (a_cmdarg_t){
                    .short_name = A_STR("b"),
                    .long_name = A_STR("arg4"),
                    .value_presence = A_CMDARG_VALUE_OPTIONAL,
                    .is_found_out = &arg4_found,
            },
            (a_cmdarg_t){
                    .long_name = A_STR("arg5"),
                    .value_presence = A_CMDARG_VALUE_OPTIONAL,
                    .is_found_out = &arg5_found,
            },
    };
    a_many_t cmdargs_count = sizeof(cmdargs) / sizeof(cmdargs[0]);

    e = a_cmdargs_parse(argv, argc, cmdargs, cmdargs_count);
    ck_assert_int_eq(e, A_ERR_OK);

    ck_assert(arg1_found);
    ck_assert(arg2_found);
    ck_assert(arg3_found);
    ck_assert(arg4_found);
    ck_assert(!arg5_found);

    ck_assert(a_str_is_empty(&arg1));
    ck_assert_msg(a_str_equal(&arg2, A_STR_P("value2")),
                  "Wrong value: `%.*s`",
                  A_STR_F(&arg2));
    ck_assert(a_str_equal(&arg3, A_STR_P("value3")));

    a_str_fin(&arg1);
    a_str_fin(&arg2);
    a_str_fin(&arg3);
}
END_TEST

static Suite *create_advc_cmdargs_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("advc-cmdargs");

    tc_core = tcase_create("parse");

    tcase_add_test(tc_core, test_cmdargs_parse);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(int argc, char *argv[])
{
    Suite *suite = create_advc_cmdargs_suite();
    SRunner *runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);

    int number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    if (number_failed != 0) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

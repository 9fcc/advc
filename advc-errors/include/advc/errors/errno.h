// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2022–2024 Andrey N. Dotsenko
 */

#ifndef H_ADVC_ERRNO_H_
#define H_ADVC_ERRNO_H_

#include <errno.h>

#ifndef __STDC_LIB_EXT1__
typedef int errno_t;
#endif

// Library error codes start from -1000 to prevent overlapping with POSIX
// positive codes and systemd negative codes.
enum {
    A_ERR_OK = 0,
    A_ERR_NOT_FOUND = -1000,
    A_ERR_BUF_OVERFLOW = -1001,
};

#define a_except(r) if (r) {\
    return r;\
}

#define a_except_goto(r, label) if (r) {\
    goto label;\
}

#endif /* H_ADVC_ERRNO_H_ */
